6. напишите команду, которая позволит перезапустить все сервисы в окружении stage в k8s кластере, исключая zookeeper, redis, sftp, rabbitmq, minio, pgbouncer, psql, postgres, kafka

Время: (2 часа 15 минут)
16:15 - 17:00
18:00 - 19:30


#!/bin/bash

deploys=`kubectl -n stage get deployments | tail -n +2 | cut -d ' ' -f 1`

for deploy in $deploys
do
  if [ "$deploy" != "zookeeper" ] && [ "$deploy" != "redis" ] && [ "$deploy" != "sftp" ] && [ "$deploy" != "rabbitmq" ] && [ "$deploy" != "minio" ] && [ "$deploy" != "pgbouncer" ] && [ "$deploy" != "psql" ] && [ "$deploy" != "postgres" ] && [ "$deploy" != "kafka" ]
  then
    kubectl -n stage rollout restart deployments/$deploy
  fi
done
