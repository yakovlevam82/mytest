7. Предоставьте конфигурационный файл роли ansible, которая позволит на "чистой" виртуалке произвести следующие действия:
создать пользователя deploy
установить vim, netstat, ltrace
разметить диск(используем lvm)
увеличить лимит на кол-во открытых файлов для пользователя deploy

Время: (2 часа 5 минут)
18:15 - 19:25
11:35 - 12:30


Последовательность действий:

Ставим ansibke и дополнительно ставим коллекцию
ansible-galaxy collection install community.general

Создаем роль:
ansible-galaxy init test-role

##
#Начало файла /etc/ansible/roles/test-role/tasks/main.yml
##
#tasks for test-role
---
- name:  "install vim"
  ansible.builtin.apt:
    name:  "vim"
    state:  "latest"
    update_cache:  true
- name:  "install ltrace"
  ansible.builtin.apt:
    name:  "ltrace"
    state:  "latest"
    update_cache:  true
- name:  "install net-tools"
  ansible.builtin.apt:
    name:  "net-tools"
    state:  "latest"
    update_cache:  true
- name:  "crete user deploy"
  ansible.builtin.user:
    name:  "deploy"
    password:  "123"
    state:  present
- name:  "create partition"
  ansible.builtin.parted:
    device: /dev/sdb
    number: 1
    flags: [ lvm ]
    state: present
- name:  "task for creating volume group"
  ansible.builtin.lvg:
    vg: sample-vg
    pvs: /dev/sdb1
    pesize: 16
- name:  "Add or modify nofile soft limit for the user deploy"
  community.general.pam_limits:
    domain: deploy
    limit_type: soft
    limit_item: nofile
    value: 64000
- name:  "Add or modify nofile hard limit for the user deploy"
  community.general.pam_limits:
    domain: deploy
    limit_type: hard
    limit_item: nofile
    value: 64000
##
#Конец файла /etc/ansible/roles/test-role/tasks/main.yml
##